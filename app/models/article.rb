class Article < ApplicationRecord
  validates :title, presence: true,
            length: {minimum: 5}

  def empty_text?
    !text.present?
  end
end
